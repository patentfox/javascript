function A() {
	this.foo = function(){
		alert("In foo");
	}
	function bar() {
		alert("In bar");
	}

	this.baz = function() {
		//foo();
		bar();
	};
}

var obj = new A();
obj.baz();

